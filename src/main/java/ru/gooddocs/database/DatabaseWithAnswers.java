package ru.gooddocs.database;


import ru.gooddocs.api.ApiMainDirectory;

import java.io.File;
import java.sql.*;

/**
 * Created by vlad on 16.04.17.
 */
public class DatabaseWithAnswers {

    private  final String  stringNameColumnInBaseImage = "image";


    private String stringPathToDatabase = "regs.db";
    private Connection connection = null;
    private Statement statement = null;

    public DatabaseWithAnswers() {
        connect();
    }




    private boolean connect(){
        stringPathToDatabase = ApiMainDirectory.getPathToMainDir() + stringPathToDatabase;
        File file = new File(stringPathToDatabase);
        boolean doIt = !file.exists();
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:"+stringPathToDatabase);
            statement = connection.createStatement();
            if (doIt){
                statement.execute("CREATE TABLE Answers (id INTEGER PRIMARY KEY AUTOINCREMENT, FirstName VARCHAR (150), SecondName VARCHAR (150), \"Group\" VARCHAR (150), DateStart DATE, DateEnd DATE, AllAnswers VARCHAR (100000));");
            }
            return true;
        } catch (SQLException e) {
            return false;
        }
    }


    public void insertAnswer(String FirstName,String SecondName,String Group,Date DateStart,
                             Date DateEnd,String AllAnswers) throws SQLException {
        String s = "INSERT INTO Answers (FirstName,SecondName,\"Group\",DateStart,DateEnd,AllAnswers) VALUES ('"+
                FirstName + "','" + SecondName + "','" + Group + "','" + DateStart + "','" + DateEnd + "','" + AllAnswers +"')";
        System.out.println(s);
        statement.execute(s);
    }


    public void close(){
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
