package ru.gooddocs.database;

import ru.gooddocs.api.ApiForCopyFiles;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by vlad on 16.04.17.
 */
public class DatabaseWithQuestion {

    private  final String  stringNameColumnInBaseImage = "image";


    private final static String stringPathToDatabase = ApiForCopyFiles.stringPathToDatabase;
    private Connection connection = null;
    private ResultSet resultSet = null;
    private Statement statement = null;

    public DatabaseWithQuestion() {
        connect();
    }





    private boolean connect(){
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:"+stringPathToDatabase);
            statement = connection.createStatement();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public List<Question> getListQuestion() throws SQLException {
        List<Question> question = new LinkedList<>();
        statement.execute("SELECT * FROM Question");
        resultSet = statement.getResultSet();
        while (resultSet.next()){
            String stringQuestion,stringAnswer1,stringAnswer2,stringAnswer3
                    ,stringAnswer4,rightAnswer;
            int intRightAnswer;
            boolean isThisAnswerNumber;
            String stringNameColumnInBaseQuestion = "question";
            stringQuestion = resultSet.getString(stringNameColumnInBaseQuestion);
            String stringNameColumnInBaseAnswer1 = "answer1";
            stringAnswer1 = resultSet.getString(stringNameColumnInBaseAnswer1);
            String stringNameColumnInBaseAnswer2 = "answer2";
            stringAnswer2 = resultSet.getString(stringNameColumnInBaseAnswer2);
            String stringNameColumnInBaseAnswer3 = "answer3";
            stringAnswer3 = resultSet.getString(stringNameColumnInBaseAnswer3);
            String stringNameColumnInBaseAnswer4 = "answer4";
            stringAnswer4 = resultSet.getString(stringNameColumnInBaseAnswer4);
            String stringNameColumnInBaseRightAnswer = "rightAnswer";
            intRightAnswer = 2 + resultSet.getInt(stringNameColumnInBaseRightAnswer);
            rightAnswer = resultSet.getString(intRightAnswer);
            String stringNameColumnInBaseIs_Number = "is_Number";
            isThisAnswerNumber = resultSet.getBoolean(stringNameColumnInBaseIs_Number);
            Question question1 = new Question(stringQuestion,stringAnswer1,stringAnswer2,
                    stringAnswer3,stringAnswer4,rightAnswer,isThisAnswerNumber);
            question.add(question1);
        }
        return question;
    }

    public void close(){
        try {
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
