package ru.gooddocs.database;

/**
 * Created by vlad on 16.04.17.
 */
public class Answer {

    String stringQuestion;
    String stringAnswer;
    String stringRightAnswer;

    public Answer(String stringQuestion, String stringAnswer, String stringRightAnswer) {
        this.stringQuestion = stringQuestion;
        this.stringAnswer = stringAnswer;
        this.stringRightAnswer = stringRightAnswer;
    }
}
