package ru.gooddocs.database;

/**
 * Created by vlad on 16.04.17.
 */
public class Question {

    private String stringQuestion;
    private String stringAnswer1;
    private String stringAnswer2;
    private String stringAnswer3;
    private String stringAnswer4;
    private String stringRightAnswer;
    private boolean isThisAnswerNumber;

    public Question(String stringQuestion, String stringAnswer1, String stringAnswer2, String stringAnswer3, String stringAnswer4, String righthAnswer4, boolean isThisAnswerNumber) {
        this.stringQuestion = stringQuestion;
        this.stringAnswer1 = stringAnswer1;
        this.stringAnswer2 = stringAnswer2;
        this.stringAnswer3 = stringAnswer3;
        this.stringAnswer4 = stringAnswer4;
        this.stringRightAnswer = righthAnswer4;
        this.isThisAnswerNumber = isThisAnswerNumber;
    }

    public boolean checkAnswer(String s){
        if (isThisAnswerNumber){
            Double aDouble = Double.parseDouble(s);
            Double max = Double.parseDouble(stringRightAnswer);
            double min = max - max * 0.05;
            max = max + max*0.05;
            if ( aDouble >= min && aDouble <= max){
                return true;
            }
        } else {
            if (s.equals(stringRightAnswer)){
                return true;
            }
        }
        return false;
    }


    public String getStringQuestion() {
        return stringQuestion;
    }

    public String getStringAnswer1() {
        return stringAnswer1;
    }

    public String getStringAnswer2() {
        return stringAnswer2;
    }

    public String getStringAnswer3() {
        return stringAnswer3;
    }

    public String getStringAnswer4() {
        return stringAnswer4;
    }

    public boolean isThisAnswerNumber() {
        return isThisAnswerNumber;
    }

    public String getRighthAnswer() {
        return stringRightAnswer;
    }

    @Override
    public String toString() {
        return "Question{" +
                "stringQuestion='" + stringQuestion + '\'' +
                ", stringAnswer1='" + stringAnswer1 + '\'' +
                ", stringAnswer2='" + stringAnswer2 + '\'' +
                ", stringAnswer3='" + stringAnswer3 + '\'' +
                ", stringAnswer4='" + stringAnswer4 + '\'' +
                ", stringRightAnswer='" + stringRightAnswer + '\'' +
                ", isThisAnswerNumber=" + isThisAnswerNumber +
                '}';
    }
}