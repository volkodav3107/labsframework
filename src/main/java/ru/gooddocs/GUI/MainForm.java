package ru.gooddocs.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Just MainGui it is a start of program
 * Created by vlad on 16.04.17.
 */
public class MainForm extends JFrame {


    private static Dimension dimension;
    final static Dimension dimensionForXp = new Dimension(1024, 768);
    private JButton jButtonFirstForm;
    private JButton jButtonSecondForm;
    private JButton jButtonThirdForm;
    private JButton jButtonForthForm;

    private MainForm() {
        super("Labs");
        dimension = getDimension();
        startGui(getContentPane());
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(dimensionForXp);
        setVisible(true);

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(MainForm::new);
    }

    private Dimension getDimension() {
        return new Dimension((int) getToolkit().getScreenSize().getWidth(),
                (int) getToolkit().getScreenSize().getHeight() - 40);
    }

    private void startGui(Container pane) {
        MyActionListener actionListener = new MyActionListener();
        GridBagLayout bagLayout = new GridBagLayout();
        pane.setLayout(bagLayout);
        GridBagConstraints constraints = new GridBagConstraints();


        jButtonFirstForm = new JButton("Для регистрации");
        jButtonFirstForm.addActionListener(actionListener);
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(5, 0, 5, 0);
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jButtonFirstForm, constraints);

        jButtonSecondForm = new JButton("Для теории");
        jButtonSecondForm.addActionListener(actionListener);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jButtonSecondForm, constraints);

        jButtonThirdForm = new JButton("Для теста");
        jButtonThirdForm.addActionListener(actionListener);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jButtonThirdForm, constraints);

        jButtonForthForm = new JButton("Для выполнения");
        jButtonForthForm.addActionListener(actionListener);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jButtonForthForm, constraints);


        pane.add(jButtonFirstForm);
        pane.add(jButtonSecondForm);
        pane.add(jButtonThirdForm);
        pane.add(jButtonForthForm);

    }

    private class MyActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == jButtonFirstForm) {
                SwingUtilities.invokeLater(SignUpForm::new);
            } else if (e.getSource() == jButtonSecondForm) {
                SwingUtilities.invokeLater(() -> {
                    SimpleSwingBrowser browser = new SimpleSwingBrowser();
                    browser.setVisible(true);
                    browser.loadURL("http://google.com");
                });
            } else if (e.getSource() == jButtonThirdForm) {
                SwingUtilities.invokeLater(TestForm::new);
            } else if (e.getSource() == jButtonForthForm) {
                SwingUtilities.invokeLater(StartForm::new);
            }
        }
    }
}
