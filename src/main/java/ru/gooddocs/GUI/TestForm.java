package ru.gooddocs.GUI;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ru.gooddocs.api.ApiForCopyFiles;
import ru.gooddocs.database.Answer;
import ru.gooddocs.database.DatabaseWithAnswers;
import ru.gooddocs.database.DatabaseWithQuestion;
import ru.gooddocs.database.Question;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import java.awt.List;

/**
 * This form need to show test and get all correct answers
 * Created by vlad on 16.04.17.
 */
public class TestForm extends JFrame {


    static String stringInputFirstName;
    static String stringInputLastName;
    static String stringInputGroup;
    static long dateStart;
    private static Dimension dimension;
    private Question question;
    private JButton jButtonReg;
    private JLabel jLabelTextQuestion;
    private JLabel jLabelTextOut;
    private JRadioButton jRadioButton1;
    private JRadioButton jRadioButton2;
    private JRadioButton jRadioButton3;
    private JRadioButton jRadioButton4;
    private ButtonGroup buttonGroup;
    private int intNowQuestion = 0;
    private List<Question> listQuestions = null;
    private List<Answer> listAnswers = null;
    private JTextField jTextField;
    private int intWasGoodAnswer = 0;
    private int intWasAllAnswer = 0;
    private String stringSelectedAnswer = "";
    private int maxQuestionOnTest = 3;
    private DatabaseWithAnswers databaseWithAnswers;
    private boolean isTest = ApiForCopyFiles.isPossible();

    TestForm() {
        super("asd");
        dimension = new Dimension((int) getToolkit().getScreenSize().getWidth(),
                (int) getToolkit().getScreenSize().getHeight() - 40);
        startDataWork();
        startGui(getContentPane());
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(MainForm.dimensionForXp);
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(TestForm::new);
    }

    private void startDataWork() {
        listAnswers = new ArrayList<>();
        ApiForCopyFiles api;
        try {
            api = new ApiForCopyFiles();
            api.startCopy();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DatabaseWithQuestion databaseWithQuestion = new DatabaseWithQuestion();
        try {
            listQuestions = databaseWithQuestion.getListQuestion();
            if (maxQuestionOnTest > listQuestions.size()) {
                maxQuestionOnTest = listQuestions.size();
            }
            Collections.shuffle(listQuestions);
            question = listQuestions.get(intNowQuestion);
            intNowQuestion++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        databaseWithQuestion.close();
        databaseWithAnswers = new DatabaseWithAnswers();
    }

    private void startGui(Container pane) {
        MyActionListener actionListener = new MyActionListener();
        buttonGroup = new ButtonGroup();
        GridBagLayout bagLayout = new GridBagLayout();
        pane.setLayout(bagLayout);
        GridBagConstraints constraints = new GridBagConstraints();

        jLabelTextQuestion = new JLabel(question.getStringQuestion());
        jLabelTextQuestion.setMinimumSize(new Dimension(500, 40));
        Font font = new Font(jLabelTextQuestion.getFont().getFontName(), Font.BOLD, 20);
        jLabelTextQuestion.setFont(font);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(5, 0, 5, 0);
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jLabelTextQuestion, constraints);
        pane.add(jLabelTextQuestion);


        jRadioButton1 = new JRadioButton(question.getStringAnswer1());
        jRadioButton1.addActionListener(actionListener);
        constraints.anchor = GridBagConstraints.EAST;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = 1;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jRadioButton1, constraints);
        buttonGroup.add(jRadioButton1);


        jRadioButton2 = new JRadioButton(question.getStringAnswer2());
        jRadioButton2.addActionListener(actionListener);
        constraints.gridy = 2;
        bagLayout.setConstraints(jRadioButton2, constraints);
        buttonGroup.add(jRadioButton2);

        jRadioButton3 = new JRadioButton(question.getStringAnswer3());
        jRadioButton3.addActionListener(actionListener);
        constraints.gridy = 3;
        bagLayout.setConstraints(jRadioButton3, constraints);
        buttonGroup.add(jRadioButton3);

        jRadioButton4 = new JRadioButton(question.getStringAnswer4());
        jRadioButton4.addActionListener(actionListener);
        constraints.gridy = 4;
        bagLayout.setConstraints(jRadioButton4, constraints);
        buttonGroup.add(jRadioButton4);

        jTextField = new JTextField();
        jTextField.setColumns(30);
        constraints.anchor = GridBagConstraints.EAST;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jTextField, constraints);

        pane.add(jRadioButton1);
        pane.add(jRadioButton2);
        pane.add(jRadioButton3);
        pane.add(jRadioButton4);
        pane.add(jTextField);

        jButtonReg = new JButton("Далее");
        jButtonReg.addActionListener(actionListener);
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.gridy = 6;
        bagLayout.setConstraints(jButtonReg, constraints);
        pane.add(jButtonReg);

        jLabelTextOut = new JLabel("Правильно 0 Всего 0");
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jLabelTextOut, constraints);
        pane.add(jLabelTextOut);

        paintRealNeedThing(question);
    }

    private void paintRealNeedThing(Question question) {
        jLabelTextQuestion.setText(question.getStringQuestion());
        if (question.isThisAnswerNumber()) {
            jRadioButton1.setVisible(false);
            jRadioButton2.setVisible(false);
            jRadioButton3.setVisible(false);
            jRadioButton4.setVisible(false);
            jTextField.setVisible(true);
        } else {
            jRadioButton1.setVisible(true);
            jRadioButton2.setVisible(true);
            jRadioButton3.setVisible(true);
            jRadioButton4.setVisible(true);
            jRadioButton1.setText(question.getStringAnswer1());
            jRadioButton2.setText(question.getStringAnswer2());
            jRadioButton3.setText(question.getStringAnswer3());
            jRadioButton4.setText(question.getStringAnswer4());
            jTextField.setVisible(false);
        }
        jTextField.setText("");
        buttonGroup.clearSelection();
        jLabelTextOut.setText("Правильно " + intWasGoodAnswer + " Всего " + intWasAllAnswer);
    }

    private void hideAll() {
        jRadioButton1.setVisible(false);
        jRadioButton2.setVisible(false);
        jRadioButton3.setVisible(false);
        jRadioButton4.setVisible(false);
        jTextField.setVisible(false);
    }


    public void setTest(boolean test) {
        isTest = test;
    }

    private class MyActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == jButtonReg) {
                if (question.isThisAnswerNumber()) {
                    stringSelectedAnswer = jTextField.getText();
                }
                listAnswers.add(new Answer(question.getStringQuestion(),
                        question.getRighthAnswer(), stringSelectedAnswer));
                if (intNowQuestion < maxQuestionOnTest) {
                    if (question.checkAnswer(stringSelectedAnswer)) {
                        intWasGoodAnswer++;
                    }
                    intWasAllAnswer++;
                    question = listQuestions.get(intNowQuestion);
                    intNowQuestion++;
                    paintRealNeedThing(question);
                } else {
                    new GsonBuilder();
                    Gson gson = new Gson();
                    String s = gson.toJson(listAnswers);
                    System.out.println(s);
                    try {
                        databaseWithAnswers.insertAnswer(stringInputFirstName, stringInputLastName, stringInputGroup,
                                new Date(dateStart), new Date(new java.util.Date().getTime()), s);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    databaseWithAnswers.close();
                    if (isTest) {
                        SwingUtilities.invokeLater(StartForm::new);
                        dispose();
                    } else {
                        jLabelTextQuestion.setText("КОНЕЦ");
                        hideAll();
                        jLabelTextOut.setText("Правильно " + intWasGoodAnswer + " Всего " + intWasAllAnswer);
                    }
                }
            } else if (e.getSource() == jRadioButton1) {
                stringSelectedAnswer = jRadioButton1.getText();
            } else if (e.getSource() == jRadioButton2) {
                stringSelectedAnswer = jRadioButton2.getText();
            } else if (e.getSource() == jRadioButton3) {
                stringSelectedAnswer = jRadioButton3.getText();
            } else if (e.getSource() == jRadioButton4) {
                stringSelectedAnswer = jRadioButton4.getText();
            }

        }
    }
}
