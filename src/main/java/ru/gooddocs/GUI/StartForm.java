package ru.gooddocs.GUI;

import ru.gooddocs.GUI.components.MyImageComponent;
import ru.gooddocs.api.ApiForCopyFiles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;

/**
 * Form with Images and descriptions
 * Created by vlad on 17.04.17.
 */
public class StartForm extends JFrame {

    private JPanel jPanelTop;
    private JPanel jPanelMiddle;
    private JPanel jPanelBottomLeft;
    private JPanel jPanelBottomRight;
    private MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getSource() instanceof MyImageComponent) {

                ((MyImageComponent) e.getSource()).setNextImage();
                System.out.println(((MyImageComponent) e.getSource()).getSize());
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    };

    public StartForm() throws HeadlessException {
        super();
        startGui(getContentPane());
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        if (MainForm.dimensionForXp == null) {
            setSize(getDimension());
        } else {
            setSize(MainForm.dimensionForXp);
        }
        setVisible(true);
    }

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        SwingUtilities.invokeAndWait(StartForm::new);
    }

    private Dimension getDimension() {
        return new Dimension((int) getToolkit().getScreenSize().getWidth(),
                (int) getToolkit().getScreenSize().getHeight() - 40);
    }

    private void startGui(Container pane) {
        GridBagLayout bagLayout = new GridBagLayout();
        pane.setLayout(bagLayout);
        GridBagConstraints constraints = new GridBagConstraints();
//        Insets insets = new Insets(5, 0, 5, 5);

        fillTopPanel();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 0;
//        constraints.insets = insets;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jPanelTop, constraints);


        fillBottomLeftPanel();
        fillMiddlePanel();
        fillBottomRightPanel();

        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(flowLayout);
        jPanel.add(jPanelBottomLeft);
        jPanel.add(jPanelMiddle);
        jPanel.add(jPanelBottomRight);


        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 1;
//        constraints.insets = insets;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jPanel,constraints);


        /*fillBottomLeftPanel();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.insets = insets;
        constraints.gridwidth = GridBagConstraints.RELATIVE;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jPanelBottomLeft, constraints);

        fillMiddlePanel();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.insets = insets;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jPanelMiddle, constraints);

        fillBottomRightPanel();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 3;
        constraints.gridy = 1;
        constraints.insets = insets;
        constraints.gridwidth = GridBagConstraints.RELATIVE;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jPanelBottomRight, constraints);*/

        pane.add(jPanelTop);
        pane.add(jPanel);
        setBackground();
//        pane.add(jPanelBottomLeft);
//        pane.add(jPanelMiddle);
//        pane.add(jPanelBottomRight);

    }


    private void fillBottomRightPanel() {
        jPanelBottomRight = new JPanel();
        MyImageComponent myImageComponent = new MyImageComponent(ApiForCopyFiles.TypeOfComponent.BOTTOM_RIGHT);
        myImageComponent.addMouseListener(mouseListener);
        jPanelBottomRight.add(myImageComponent);
        jPanelBottomRight.setVisible(true);
    }

    private void fillBottomLeftPanel() {
        jPanelBottomLeft = new JPanel();
        MyImageComponent myImageComponent = new MyImageComponent(ApiForCopyFiles.TypeOfComponent.BOTTOM_LEFT);
        myImageComponent.addMouseListener(mouseListener);
        jPanelBottomLeft.add(myImageComponent);
        jPanelBottomLeft.setVisible(true);
    }

    private void fillMiddlePanel() {
        jPanelMiddle = new JPanel();

        MyImageComponent myImageComponent = new MyImageComponent(ApiForCopyFiles.TypeOfComponent.MIDDLE_IMAGE);
        myImageComponent.addMouseListener(mouseListener);
        jPanelMiddle.add(myImageComponent);
        jPanelMiddle.setVisible(true);
    }

    private void fillTopPanel() {
        jPanelTop = new JPanel();
        MyImageComponent myImageComponent = new MyImageComponent(ApiForCopyFiles.TypeOfComponent.TOP_IMAGE, new Dimension(1024, 384));
        myImageComponent.addMouseListener(mouseListener);
        jPanelTop.add(myImageComponent);
        jPanelTop.setVisible(true);
    }

    private void setBackground() {
        jPanelTop.setBackground(Color.black);
        jPanelBottomLeft.setBackground(Color.cyan);
        jPanelBottomRight.setBackground(Color.GREEN);
        jPanelMiddle.setBackground(Color.RED);
    }
}
