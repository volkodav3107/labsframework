package ru.gooddocs.GUI.components;

import ru.gooddocs.api.ApiForCopyFiles;

import javax.swing.*;
import java.awt.*;

/**
 * Class which need to show Images
 * Created by vlad on 17.04.17.
 */
public class MyImageComponent extends JPanel {

    //    private Image image;
    private JLabel image;
    private int counter = 0;
    private java.util.List<String> stringList = ApiForCopyFiles.getListImages(
            ApiForCopyFiles.TypeOfComponent.TOP_IMAGE);

    private Dimension dimension;


    public MyImageComponent(ApiForCopyFiles.TypeOfComponent typeOfComponent) {
        super();
        stringList = ApiForCopyFiles.getListImages(typeOfComponent);
        init();
    }

    public MyImageComponent(ApiForCopyFiles.TypeOfComponent typeOfComponent, Dimension dimension) {
        super();
        this.dimension = dimension;
        stringList = ApiForCopyFiles.getListImages(typeOfComponent);
        init();
    }

    public MyImageComponent() {
        super();
        init();
    }

    private void init() {
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        setLayout(flowLayout);
        if (dimension != null) {
            setMaximumSize(dimension);
            setPreferredSize(dimension);
//            image.setMaximumSize(dimension);
        }
        image = new JLabel();
        image.setMaximumSize(dimension);
        add(image);
        changeImage(0);
        setVisible(true);
    }




    public void setNextImage() {
        if (counter < stringList.size() - 1) {
            counter++;
        } else {
            counter = 0;
        }
        changeImage(counter);
    }

    private void changeImage(int i) {
        image.setIcon(new ImageIcon(stringList.get(i)));
        System.out.println("image = " + image.getSize());
    }

}
