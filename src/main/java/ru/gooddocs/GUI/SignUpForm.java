package ru.gooddocs.GUI;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Start for with Registration
 * Created by vlad on 16.04.17.
 */
public class SignUpForm extends JFrame {


    private String stringStartReg = "Регистрация";
    private String stringStartFirstName = "Введите ваше имя";
    private String stringStartLastName = "Введите вашу фамилию";
    private String stringStartGroup = "Введите вашу группу";
    private String stringInputFirstName;
    private String stringInputLastName;
    private String stringInputGroup;
    private long dateStart;
    private JTextField jTextFieldFirstName;
    private JTextField jTextFieldLastName;
    private JTextField jTextFieldGroup;
    private final Class nextClassForOpen;

    private JButton jButtonReg;

    private static Dimension dimension;

    SignUpForm() {
        super("Labs");
        dimension = getDimension();
        startGui(getContentPane());
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(MainForm.dimensionForXp);
        setVisible(true);
        nextClassForOpen = TestForm.class;
    }

    private SignUpForm(Class nextClassForOpen) throws HeadlessException {
        super("Labs");
        dimension = getDimension();
        startGui(getContentPane());
        pack();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setSize(MainForm.dimensionForXp);
        setVisible(true);

        this.nextClassForOpen = nextClassForOpen;
    }

    private Dimension getDimension() {
        return new Dimension((int) getToolkit().getScreenSize().getWidth(),
                (int) getToolkit().getScreenSize().getHeight() - 40);
    }


    private void startGui(Container pane) {
        MyActionListener actionListener = new MyActionListener();
        GridBagLayout bagLayout = new GridBagLayout();
        pane.setLayout(bagLayout);
        GridBagConstraints constraints = new GridBagConstraints();

        JLabel jLabel = new JLabel(stringStartReg);
        Font font = new Font(jLabel.getFont().getFontName(), Font.BOLD, 20);
        jLabel.setFont(font);
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(5, 0, 5, 0);
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jLabel, constraints);
        pane.add(jLabel);

        JLabel jLabelFirstName = new JLabel(stringStartFirstName);
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;
        bagLayout.setConstraints(jLabelFirstName, constraints);
        pane.add(jLabelFirstName);

        jTextFieldFirstName = new JTextField(30);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jTextFieldFirstName, constraints);
        pane.add(jTextFieldFirstName);

        JLabel jLabelLastName = new JLabel(stringStartLastName);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jLabelLastName, constraints);
        pane.add(jLabelLastName);


        jTextFieldLastName = new JTextField(30);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jTextFieldLastName, constraints);
        pane.add(jTextFieldLastName);

        JLabel jLabelGroup = new JLabel(stringStartGroup);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jLabelGroup, constraints);
        pane.add(jLabelGroup);


        jTextFieldGroup = new JTextField(30);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jTextFieldGroup, constraints);
        pane.add(jTextFieldGroup);

        jButtonReg = new JButton(stringStartReg);
        jButtonReg.addActionListener(actionListener);
        constraints.gridy = GridBagConstraints.RELATIVE;
        bagLayout.setConstraints(jButtonReg, constraints);
        pane.add(jButtonReg);

    }


    private class MyActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == jButtonReg) {
                if (checkDates()) {
                    TestForm.stringInputFirstName = stringInputFirstName;
                    TestForm.stringInputLastName = stringInputLastName;
                    TestForm.stringInputGroup = stringInputGroup;
                    TestForm.dateStart = dateStart;
                    if (nextClassForOpen != null) {
                        SwingUtilities.invokeLater(() -> {
                            try {
                                nextClassForOpen.newInstance();
                            } catch (InstantiationException | IllegalAccessException e1) {
                                e1.printStackTrace();
                            }
                        });
                    }
                    dispose();
                } else {
                    jTextFieldGroup.setText("Введите группу");
                }
            }
        }
    }

    private boolean checkDates() {
        if (jTextFieldFirstName.isValid() && jTextFieldGroup.isValid() && jTextFieldLastName.isValid()) {
            if (!jTextFieldFirstName.getText().equals("") && !jTextFieldLastName.getText().equals("") && !jTextFieldGroup.getText().equals("")) {
                stringInputFirstName = jTextFieldFirstName.getText();
                stringInputLastName = jTextFieldLastName.getText();
                stringInputGroup = jTextFieldGroup.getText();
                dateStart = new Date().getTime();
                return true;
            }
        }

        return false;
    }

    public   static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new SignUpForm(TestForm.class));
    }
}
