package ru.gooddocs.api;

/**
 * This class need only for One function - return path to Main program directory
 * Created by vlad on 17.04.17.
 */
public class ApiMainDirectory {


    /**
     * Should change if your want change main program directory
     */
    private static final String stringProgramFullName = "lab2017-boyko";

    /**
     * Attention already have / at end path
     *
     * @return pathToMainDirectory which dependents from OS
     * for example "/home/vlad/.lab2017-1/" for linux
     */
    public static String getPathToMainDir() {
        StringBuilder builder = new StringBuilder().append(System.getProperty("user.home"));
        switch (findOSType()) {
            case MAC:
                break;
            case UNIX:
                builder.append("/")
                        .append(".")
                        .append(stringProgramFullName)
                        .append("/");
                break;
            case WINDOWS:
                builder.append(stringProgramFullName)
                        .append("/");
                break;
            default:
        }
        System.out.println(builder.toString());
        return builder.toString();

    }


    private static OS findOSType() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") > 0) {
            return OS.WINDOWS;
        } else if (os.indexOf("mac") > 0) {
            return OS.MAC;
        } else return OS.UNIX;

    }

    private enum OS {WINDOWS, UNIX, MAC}
}
