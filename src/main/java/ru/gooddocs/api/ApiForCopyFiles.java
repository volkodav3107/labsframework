package ru.gooddocs.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by vlad on 16.04.17.
 */
public class ApiForCopyFiles {

    public static String stringPathToDatabase = "";
    private static boolean isPossible = true;
    private static String pathToMainDir;
    private static List<String> stringListTopImage = Collections.unmodifiableList(
            Arrays.asList("images/top_image_1.jpg", "images/top_image_2.jpg"));
    private static List<String> stringListMiddleImage = Collections.unmodifiableList(
            Arrays.asList("images/middle_image_1.png", "images/middle_image_2.png", "images/middle_image_3.png"));
    private static List<String> stringListBottomLeftImage = Collections.unmodifiableList(
            Arrays.asList("images/bottom_left_image_1.jpg", "images/bottom_left_image_2.jpg"));
    private static List<String> stringListBottomRightImage = Collections.unmodifiableList(
            Arrays.asList("images/bottom_right_image_1.jpg", "images/bottom_right_image_2.jpg"));



    public ApiForCopyFiles() throws IOException {
        pathToMainDir = ApiMainDirectory.getPathToMainDir();
        copyAllToMainDir();

    }

    public static List<String> getListImages(TypeOfComponent typeOfComponent) {
        switch (typeOfComponent) {
            case TOP_IMAGE:
                return getListWithMainDir(stringListTopImage);
            case MIDDLE_IMAGE:
                return getListWithMainDir(stringListMiddleImage);
            case BOTTOM_LEFT:
                return getListWithMainDir(stringListBottomLeftImage);
            case BOTTOM_RIGHT:
                return getListWithMainDir(stringListBottomRightImage);
            default:
                return new ArrayList<>();
        }
    }

    public static boolean isPossible() {
        return isPossible;
    }


    /**
     * @param path  Path to resource as "/1.db"
     * @param exist ".db"
     */
    private String copyToTempFolder(String path, String exist) throws IOException {
        InputStream stream = getClass().getResourceAsStream(path);
        String tempFolderPath = System.getProperty("java.io.tmpdir");
        Path tempFile = Files.createTempFile(Paths.get(tempFolderPath), null, exist);
        Files.copy(stream, tempFile, StandardCopyOption.REPLACE_EXISTING);
        return tempFile.toString();
    }

    public void startCopy() throws IOException {
        if (isPossible) {
            stringPathToDatabase = copyToTempFolder("/test.db", ".db");
            isPossible = false;
        }
    }


    private void copyAllToMainDir(){
        try {
            copyToMainDir(stringListTopImage);
            copyToMainDir(stringListMiddleImage);
            copyToMainDir(stringListBottomLeftImage);
            copyToMainDir(stringListBottomRightImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * @param list
     * @throws IOException
     */
    private void copyToMainDir(List<String> list) throws IOException {
        for (String s :
                list) {
            InputStream inputStream = getClass().getResourceAsStream("/" + s);
            Path path = Paths.get(pathToMainDir + s);
            File file = path.toFile();
            file.mkdirs();
            Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        }

    }

    private static List<String> getListWithMainDir(List<String> stringList) {
        List<String> list = new ArrayList<>();
        String pathToMainDir = ApiMainDirectory.getPathToMainDir();
        for (String s :
                stringList) {
            list.add(pathToMainDir + s);
        }
        return list;
    }


    public enum TypeOfComponent {TOP_IMAGE, MIDDLE_IMAGE, BOTTOM_LEFT, BOTTOM_RIGHT}


}
